# Front-End ![front end build status](https://codebuild.us-west-2.amazonaws.com/badges?uuid=eyJlbmNyeXB0ZWREYXRhIjoibHpFcDlDMXhOWlQzQU9lRW5HZURuaXI0ZWFsQUJQaWR2YWJYLzBydnRNZHdXS1VuOGZsYUN6dkV2V0hYbTcyT0c3NlVOeTcyUmQ5YWN4R3R2MEFOYlhFPSIsIml2UGFyYW1ldGVyU3BlYyI6ImRtSVBtMXJyZWs5SVpvNHEiLCJtYXRlcmlhbFNldFNlcmlhbCI6MX0%3D&branch=master)

This project is written with ReactJS meant to be the Web application to manage the customer features.

# How to run manually
- Install Node.js 10 ([Link](https://nodejs.org/en/download/releases/)).
- set `REACT_APP_API_URL` environment variable with the project `/api` address.
    - example: `export REACT_APP_API_URL=http://0.0.0.0:4000:8080/api/v1/`
- run `npm install` to install depenencies
- run `npm start` to start the project.