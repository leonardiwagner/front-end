FROM node:10-alpine3.9

ARG REACT_APP_API_URL
ENV REACT_APP_API_URL $REACT_APP_API_URL

RUN mkdir /front-end
COPY . /front-end
WORKDIR /front-end

RUN npm i
RUN npm run build

FROM nginx:latest
COPY --from=0 /front-end/build/ /usr/share/nginx/html
COPY --from=0 /front-end/nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 5000