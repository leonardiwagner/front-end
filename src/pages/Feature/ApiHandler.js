import { useState } from 'react'

import FeatureRepository from '../../repositories/Feature'
import CustomerRepository from '../../repositories/Customer'

const ApiHandler = ({ match, setIsLoading, setShowSucessMessage, setShowErrorMessage}) => {
  const [customers, setCustomers] = useState([])
  const [feature, setFeature] = useState({
    id: match.params.featureId,
    name: '',
    displayName: '',
    description: '',
    expiration: undefined,
    isInverted: false,
    customers: []
  })

  const loadCustomers = () => CustomerRepository.get()
    .then((customers) => {
      setCustomers(customers);
    })

  const loadFeature = () => {
    if(feature.id) {
      return FeatureRepository.get(feature.id).then((feature) => {
        setFeature(feature)
      })
    }
  }

  const loadDataFromApi = () => {
    return loadCustomers()
      .then(() => loadFeature())
      .then(() => setIsLoading(false))
      .catch((error) => {
        console.error(error)
        setShowErrorMessage(true)
      })
  }

  const save = () => {
    setIsLoading(true)

    const saveFunction = feature.id ? FeatureRepository.update : FeatureRepository.add
    
    return saveFunction(feature)
      .then(({ id }) => setFeature({...feature, id}))
      .then(() => setIsLoading(false))
      .then(() => setShowSucessMessage(true))
      .catch((error) => {
        console.error(error)
        setShowErrorMessage(true)
      })
  }
 
  const remove = () => {
    setIsLoading(true)

    return FeatureRepository.remove(feature)
      .then((response) => response)
      .catch((error) => {
        console.error(error)
        setShowErrorMessage(true)
      })
  }

  return {
    feature,
    setFeature,
    customers,
    save,
    remove,
    loadDataFromApi
  }
}

export default ApiHandler