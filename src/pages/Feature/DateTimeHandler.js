import { useState } from 'react'

import DateTime from '../../domain/DateTime'

const DateTimeHandler = ({ setFeature, feature }) => {
  const [expirationDate, setExpirationDate] = useState(null)
  const [expirationTime, setExpirationTime] = useState(null)

  const onDateChange = (value, isTimeOnly = false) => {
    if(!value) {
      return setFeature({...feature, expiration: undefined })
    }

    
    const isDateFilled = !isTimeOnly && expirationDate
    const isTimeFilled = isTimeOnly && expirationTime

    if(isDateFilled || isTimeFilled){
      const date = !isTimeOnly ?
        DateTime.getDateString(expirationDate) :
        DateTime.getDateString(value)
      const time = isTimeOnly ?
        DateTime.getTimeString(value) :
        DateTime.getTimeString(expirationTime)
      
      const expiration = date + " " + time
      return setFeature({...feature, expiration })
    }

    const dateOnly = DateTime.getDateString(value) + " 00:00"
    return setFeature({...feature, expiration: dateOnly })
  }

  return {
    onDateChange,
    expirationDate,
    expirationTime,
    setExpirationDate,
    setExpirationTime
  }
}

export default DateTimeHandler