import { useState } from 'react'

const MessagesHandler = () => {
  const [isLoading, setIsLoading] = useState(true)
  const [showDeleteConfirmation, setShowDeleteConfirmation] = useState(false)
  const [showSucessMessage, setShowSucessMessage] = useState(false)
  const [showErrorMessage, setShowErrorMessage] = useState(false)
  
  const closeMessage = () => {
    setShowSucessMessage(false)
    setShowErrorMessage(false)
  }

  return {
    isLoading,
    setIsLoading,
    showDeleteConfirmation,
    setShowDeleteConfirmation,
    showSucessMessage,
    setShowSucessMessage,
    showErrorMessage,
    setShowErrorMessage,
    closeMessage
  }
}

export default MessagesHandler