import React, {useState, useEffect} from 'react'

import DateFnsUtils from '@date-io/date-fns'
import {
  KeyboardDatePicker,
  KeyboardTimePicker,
  MuiPickersUtilsProvider,
} from '@material-ui/pickers'

import { CircularProgress, FormControlLabel, Button, TextField, Switch }  from '@material-ui/core'

import CustomerToggle from '../../components/CustomerToggle'
import ConfirmationDialog from '../../components/ConfirmationDialog'
import MessageAlert from '../../components/MessageAlert'

import ApiHandler from './ApiHandler'
import DateTimeHandler from './DateTimeHandler'
import MessageHandler from './MessageHandler'

import './Feature.scss'

function Feature({ match }) {
  const {
    isLoading, setIsLoading, showSucessMessage, setShowErrorMessage, showErrorMessage,
    closeMessage, showDeleteConfirmation, setShowSucessMessage, setShowDeleteConfirmation
  } = MessageHandler()

  const {
    feature, setFeature, customers, save, remove, loadDataFromApi
  } = ApiHandler({ match, setIsLoading, setShowSucessMessage, setShowErrorMessage })
  
  const {
    expirationDate, expirationTime, onDateChange, setExpirationDate, setExpirationTime
  } = DateTimeHandler({ setFeature, feature })

  useEffect(() => {
    loadDataFromApi()
  }, [])

  useEffect(() => {
    if(feature.expiration){
      const date = new Date(feature.expiration)
      setExpirationDate(date)
      return setExpirationTime(date)
    }

    setExpirationDate(null)
    return setExpirationTime(null)
  }, [feature])

  const handleChange = (event) => {
    if(event.target.name === 'isInverted') {
      return setFeature({...feature, isInverted: !feature.isInverted})
    }
    return setFeature({...feature, [event.target.name]: event.target.value})
  }

  const onSelectCustomer = (ids) => {
    const customersObject = ids.map((id) => ({ id }))
    setFeature({...feature, customers: customersObject})
  }

  const validateToSave = () => {
    if(feature.name.length > 0) {
      return save()
    }

    setShowErrorMessage(true)
  }

  const deleteFeature = () => {
    setShowDeleteConfirmation(false)
    remove()
      .then(() => window.location = '/')
  }

  return (
    <div className="feature">
      <h2>
        { feature.id ? `Edit Feature ${feature.displayName || feature.name}` : "Add New Feature" }
      </h2>
        <div className={`feature__form${isLoading ? '--loading' : ''}`} autoComplete="off">
          <div className="feature__form__row">
            <TextField autoFocus onChange={handleChange} name="name"  value={feature.name || ''} label="Technical Name" variant="outlined" error={!feature.name.length} helperText={!feature.name.length && 'Required Field'}/>
            <TextField onChange={handleChange} name="displayName" value={feature.displayName || ''} id="standard-basic" label="Display Name" variant="outlined" />
          </div>
          <div className="feature__form__row">
            <TextField onChange={handleChange} name="description" value={feature.description || ''} label="Description" variant="outlined" className="full-size"/>
          </div>
          <div className="feature__form__row">
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <KeyboardDatePicker clearable format="MM/dd/yyyy" name="expirationDate" label="Expiration Date" value={expirationDate} onChange={onDateChange} inputVariant="outlined" />
              <KeyboardTimePicker name="expirationTime" ampm={false} label="Expiration Time" value={expirationTime} onChange={(value) => onDateChange(value, true)} inputVariant="outlined" disabled={!expirationDate} />
            </MuiPickersUtilsProvider>
          </div>
          <div className="feature__form__row">
            <CustomerToggle customers={customers} selectedCustomers={feature.customers} onSelectCustomer={onSelectCustomer} className="full-size" />
          </div>
          <div className="feature__form__row full-size">
            <FormControlLabel
                control={
                  <Switch
                    onChange={handleChange}
                    name="isInverted"
                    checked={feature.isInverted || false}
                    color="primary"
                  />
                }
                label="Invert Enabled Customers Rule"
                className="full-size"
              />
          </div>
        </div>
        <div className="feature__buttons">
          {
            !isLoading &&
            <Button variant="contained" color="primary" onClick={() => validateToSave()}>
              Save
            </Button>
          }
          {
            !isLoading && feature.id &&
            <Button variant="contained" color="secondary" onClick={() => setShowDeleteConfirmation(true)}>
              Delete
            </Button>
          }
          <Button variant="contained" href="/">Back</Button>
        </div>
    
        <MessageAlert
          successMessage="Feature saved!"
          errorMessage={!feature.name.length ? "Please insert a Name" : "Sorry, we can't save features right now, please try again."}
          showSucess={showSucessMessage}
          showError={showErrorMessage}
          closeMessage={closeMessage}
        />
        {
          isLoading &&
          <CircularProgress />
        }
        <ConfirmationDialog
          show={showDeleteConfirmation}
          title="Delete Feature"
          message="Do you really want to delete this feature?"
          onConfirm={deleteFeature}
          onCancel={() => setShowDeleteConfirmation(false)}
        />
    </div>
  )
}

export default Feature


