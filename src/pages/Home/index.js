import React, { useState, useEffect } from 'react'
import { List, CircularProgress }  from '@material-ui/core'
import './Home.scss'

import { Button }  from '@material-ui/core'
import AddIcon from '@material-ui/icons/Add'

import MessageAlert from '../../components/MessageAlert'
import FeatureListItem from '../../components/FeatureListItem'
import FeatureRepository from '../../repositories/Feature'

const Home = () => {
  const [features, setFeatures] = useState([])
  const [isLoading, setIsLoading] = useState(true)
  const [showErrorMessage, setShowErrorMessage] = useState(false)

  useEffect(() => {
    const loadFeatures = () => FeatureRepository.getAll()
      .then((result) => setFeatures(result))
      .then(() => setIsLoading(false))
      .catch((error) => {
        console.error(error)
        setShowErrorMessage(true)
      })

    loadFeatures()

  }, [])

  return (
    <section>
      <div className="home-header">
        <h2>Features</h2>
        <Button
          className="home-header__button"
          variant="contained"
          color="primary"
          size="medium"
          startIcon={<AddIcon />}
          href="/feature"
          disabled={isLoading}
        >
          Add Feature
        </Button>
      </div>
      <List className={`home-list${isLoading ? '--loading' : ''}`} component="nav" aria-label="main mailbox folders">
        {
          features.map((feature, i) =>
            <FeatureListItem feature={feature} key={i} />
          )
        }
      </List>
      {
        isLoading &&
        <CircularProgress />
      }
      <MessageAlert
        errorMessage="Sorry, we can't load features right now, please try again."
        showError={showErrorMessage}
      />
    </section>
  )
}

export default Home
