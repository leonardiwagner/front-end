import React from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import { AppBar, Toolbar, Typography }  from '@material-ui/core'

import Home from './pages/Home'
import Feature from './pages/Feature'

const App = () =>
  <Router>
    <AppBar position="static">
      <Toolbar>
        <Typography variant="h6">
          SC Assessment
        </Typography>
        
      </Toolbar>
    </AppBar>

    <div className="wrapper">
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/feature/:featureId" component={Feature} />
        <Route path="/feature" component={Feature} />
      </Switch>
    </div>
  </Router>

export default App