import React from 'react'
import { ListItem, Avatar, Button, Link }  from '@material-ui/core'
import { Edit } from '@material-ui/icons'

import DateTime from '../../domain/DateTime'
import Formatter from '../../domain/Formatter'

import './FeatureListItem.scss'

const FeatureListItem = ({ feature }) => {
  const featureName = feature.displayName || feature.name

  return (
    <ListItem className="feature-list" button component="a" href={`feature/${feature.id}`}>
      <div className="avatar">
        <Avatar>{ Formatter.getAvatarText(featureName)}</Avatar>
      </div>
      <div className="details">
        <div>{ featureName }</div>
        <div><small>Expires On: <em>{ feature.expiration ? DateTime.getDateString(feature.expiration) : "Never" }</em></small></div>
        <div><small>Updated On: { DateTime.getDateString(feature.updated) }</small></div>
        <div><small>Customers: { feature.customers && feature.customers.length }</small></div>
      </div>
      <div className="button" >
        <Button
          variant="contained"
          size="medium"
          startIcon={<Edit />}
          href={`feature/${feature.id}`}
        >
          Edit
        </Button>
      </div>
    </ListItem>
  )
}

export default FeatureListItem
