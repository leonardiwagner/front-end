import React, { useState, useEffect } from 'react';
import Toggle from '../Toggle'

import Formatter from '../../domain/Formatter'

import './CustomerToggle.scss';

const CustomerToggle = ({ customers, selectedCustomers, onSelectCustomer }) => {
  const [items, setItems] = useState([]);
  const [enabledIds, setEnabledIds] = useState([]);
  const [disabledIds, setDisabledIds] = useState([]);
  
  useEffect(() => {
    setDisabledIds(customers.map(({id}) => id))
    setItems(customers)
  }, [customers])

  useEffect(() => {
    const enabledItems = selectedCustomers.map(({id}) => id)
    const disabledItems = items
      .map(({id}) => id)
      .filter((id) => !enabledItems.includes(id))

    setEnabledIds(enabledItems)
    setDisabledIds(disabledItems)
  }, [selectedCustomers])

  const onClick = async(id, isRemoval = true) => {
    const { addList, removeList, addFunction, removeFunction }
    = isRemoval ? {
      addList: disabledIds,
      removeList: enabledIds,
      addFunction: setDisabledIds,
      removeFunction: setEnabledIds,
    } : {
      addList: enabledIds,
      removeList: disabledIds,
      addFunction: setEnabledIds,
      removeFunction: setDisabledIds
    }

    const removedList = removeList.filter((item) => item !== id)
    const addedList = [...addList, id]
    
    removeFunction(removedList)
    addFunction(addedList)

    const callbackList = isRemoval ? removedList : addedList
    if(onSelectCustomer) onSelectCustomer(callbackList)
  }

  const getName = (id) => {
    const customer = items.find((item) => item.id === id)
    if(customer) return customer.name
  }

  const getAvatarText = (id) => Formatter.getAvatarText(getName(id))

  return (
    <div className="customer-toggle">
      <h4>Enabled Customers:</h4>
      <div className="customer-toggle__toggles">
        {
          enabledIds.map((id, i) =>
            <Toggle
              id={id}
              key={i}
              onClick={() => onClick(id, true)}
              getName={getName}
              getAvatarText={getAvatarText}
              isDelete
            />
          )
        }
        {
          enabledIds.length === 0 &&
          <h5>Please select a disabled Customer</h5>
        }
      </div>

      <h4>Disabled Customers:</h4>
      <div className="customer-toggle__toggles">
        {
          disabledIds.map((id, i) =>
            <Toggle
              id={id}
              onClick={() => onClick(id, false)}
              getName={getName}
              getAvatarText={getAvatarText}
              key={i}
            />
          )
        }
        {
          disabledIds.length === 0 &&
          <h5>All customers enabled</h5>
        }
      </div>
    </div>
  );
}

export default CustomerToggle;
