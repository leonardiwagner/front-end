import React from 'react'
import { Snackbar } from '@material-ui/core'
import MuiAlert from '@material-ui/lab/Alert'

const MessageAlert = ({ showSucess, showError, successMessage, errorMessage, closeMessage }) => 
  <div>
    <Snackbar open={showSucess} autoHideDuration={6000} onClose={closeMessage} >
      <MuiAlert elevation={6} variant="filled" severity="success">
        {successMessage}
      </MuiAlert>
      </Snackbar>
      <Snackbar open={showError} autoHideDuration={6000} onClose={closeMessage} >
      <MuiAlert elevation={6} variant="filled" severity="error">
        {errorMessage}
      </MuiAlert>
    </Snackbar>
  </div>

export default MessageAlert


