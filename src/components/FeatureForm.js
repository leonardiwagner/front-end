import React from 'react';
import './FeatureForm.css';
import { Button, TextField, Switch, Select, MenuItem }  from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';



const FeatureForm = () => {

  return (
    <div className="FeatureForm">
      <form autoComplete="off">
        <TextField id="standard-basic" label="Display Name" variant="outlined" />
        <TextField id="filled-basic" label="Technical Name" variant="outlined" required />
        <TextField
          id="date"
          label="Expires On"
          type="datetime-local"
          variant="outlined"
          InputLabelProps={{
            shrink: true,
          }}
        />
        <TextField id="outlined-basic" label="Description" variant="outlined" />
        <Switch required checked={false} name="checkedA" inputProps={{ 'aria-label': 'secondary checkbox' }} />
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
        >
          <MenuItem value={10}>Ten</MenuItem>
          <MenuItem value={20}>Twenty</MenuItem>
          <MenuItem value={30}>Thirty</MenuItem>
        </Select>
      </form>
      <Button variant="contained" color="primary">
        Hello World
      </Button>
    </div>
  );
}

export default FeatureForm;
