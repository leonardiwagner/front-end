import React from 'react'
import { Avatar, Chip } from '@material-ui/core'
import { HighlightOff, Done } from '@material-ui/icons'

const Toggle = ({ onClick, id, getName, getAvatarText, isDelete = false }) => 
  <Chip
    color={ isDelete ? "secondary" : "primary" }
    clickable
    onClick={() => onClick(id)} avatar={<Avatar>{getAvatarText(id)}</Avatar>}
    onDelete={() => onClick(id)} avatar={<Avatar>{getAvatarText(id)}</Avatar>}
    label={getName(id)}
    key={id}
    deleteIcon={isDelete ? <HighlightOff /> : <Done />}
  />


export default Toggle
