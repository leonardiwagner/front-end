import React, {useState, useEffect} from 'react'
import { Button, Dialog, DialogTitle, DialogContent, DialogActions }  from '@material-ui/core'

const ConfirmationDialog = ({ title, message, onConfirm, onCancel, show }) => {
  const [isVisible, setIsVisible] = useState(false); 

  useEffect(() => {
    setIsVisible(show)
  }, [show])

  return (
    <Dialog
    disableBackdropClick
    disableEscapeKeyDown
    maxWidth="xs"
    aria-labelledby="confirmation-dialog-title"
    open={isVisible}
    >
      <DialogTitle id="confirmation-dialog-title">{title}</DialogTitle>
      <DialogContent dividers>
        {message}
      </DialogContent>
      <DialogActions>
        <Button onClick={onCancel} autoFocus color="primary">
          Cancel
        </Button>
        <Button onClick={onConfirm}  color="primary">
          Ok
        </Button>
      </DialogActions>
    </Dialog>
  )
}  


export default ConfirmationDialog
