const getDateString = (value) => {
  const date = value instanceof Date ? value : new Date(value)
  return  `${date.getFullYear()}-${('0' + (date.getMonth()+1)).slice(-2)}-${('0' + date.getDate()).slice(-2)}`
}

const getTimeString = (date) =>
`${('0' + date.getHours()).slice(-2)}:${('0' + date.getMinutes()).slice(-2)}`

export default {
  getDateString, getTimeString
}