const getAvatarText = (text) => {
  const name = text.toUpperCase()
  if(name.includes(' ')){
    const [first, second, ...others] = name.split(' ')
    return first.substr(0, 1) + second.substr(0, 1)
  }

  return name.substr(0, 2)
}

export default {
  getAvatarText
}