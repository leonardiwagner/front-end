const API_URL = process.env.REACT_APP_API_URL

const handleResponse = (response) => {
  if(response.status !== 200){
    throw new Error(response);
  }

  return response.json()
}

const getAll = () =>
  fetch(`${API_URL}/features`)
  .then(handleResponse)

const get = (featureId) =>
  fetch(`${API_URL}/feature/${featureId}`)
  .then(handleResponse)
  .then((feature) => ({
    id: feature.id || undefined,
    name: feature.name || undefined,
    displayName: feature.displayName || undefined,
    description: feature.description || undefined,
    isInverted: feature.isInverted || false,
    expiration: feature.expiration || undefined,
    customers: feature.customers || []
  }))

const add = (feature) => 
  fetch(`${API_URL}/feature`, {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(feature)
  })
  .then(handleResponse)

const update = (feature) => 
  fetch(`${API_URL}/feature/${feature.id}`, {
    method: 'PUT',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(feature)
  })
  .then(handleResponse)

const remove = (feature) => 
  fetch(`${API_URL}/feature/${feature.id}`, {
    method: 'DELETE'
  })
  .then(handleResponse)
  
export default { get, getAll, add, remove, update }
