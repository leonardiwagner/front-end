const API_URL = process.env.REACT_APP_API_URL

const get = () =>
  fetch(`${API_URL}/customers`)
  .then((response) =>  response.json())

export default { get }
